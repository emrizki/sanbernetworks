@extends('stisla.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
        <div class="card">
            <div class="card-header d-flex flex-column justify-content-center">
                <div class="p-2"><h3 class="card-title">Profile</h3></div>
                <div class="p-2"><img src="{{asset('/images/avatar-1.png')}}"  alt="my profile" class="img-circle img-bordered-sm rounded-circle" width="200px"></div>
                <div class="p-2 d-flex">
                      <button type="button" class="btn btn-primary">
                        Followers<span class="badge badge-transparent">5</span>
                      </button>
                      <button type="button" class="btn btn-primary">
                        Following<span class="badge badge-transparent">5</span>
                      </button>
                </div>
                <div class="btn-group p-2">
                      <button type="button" class="btn btn-danger">Split Dropdown</button>
                      <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-expanded="false">
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(119px, 35px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <a class="dropdown-item" href="/profile/edit">Edit Profile</a>
                        <a class="dropdown-item" href="/posts/create">Create Post</a>
                      </div>
                    </div>
            </div>
              <!-- /.card-header -->
              <div class="card">
                  <div class="card-header">
                    <h4>Profile Bio</h4>
                    <div class="card-header-action">
                      <a data-collapse="#mycard-collapse" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
                    </div>
                  </div>
                  <div class="collapse show" id="mycard-collapse" style="">
                    <div class="card-body">
                      <p>Nama Lengkap: {{$profile->full_name}}</p>
                      <p>Username: {{$profile->username}}</p>
                      <p>Email: {{$profile->email}}</p>
                    </div>
                  </div>
                </div>
              <!-- /.card-body -->
        </div>
    </div>
@endsection

@push('scripts')
<script src="../dist/js/scripts.js"></script>
<script src="../dist/js/custom.js"></script>
@endpush
