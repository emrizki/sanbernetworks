@extends('stisla.master')

@section('content')
<div class="card">
    <div class="card-header">
      <h4>Create Biodata</h4>
    </div>
    <form role="form" action="/profile" method="POST">
    @csrf   
    @method('put')    
    <div class="card-body">
          <div class="form-group">
            <label for="nama">Nama Lengkap</label>
            <input type="text" class="form-control" name="full_name" value="{{ old('full_name','')}}" id="nama" placeholder="Nama lengkap">
          </div>
          <div class="input-group mb-2">
              <div class="input-group-prepend">
                <div class="input-group-text">@username</div>
              </div>
              <input type="text" class="form-control" name="username" id="inlineFormInputGroup" placeholder="Username">
            </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" name="email" value="{{ old('email','')}}" id="email" placeholder="Email">
          </div>
          <form action="{{ url('/photo/') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <label for="image">Foto</label>
            <input type="file" class="form-control" name="photo" id="image" value="Upload">
          </form>
        <div class="card-footer text-right">
          <button class="btn btn-primary mr-1" type="submit">Submit</button>
          <button class="btn btn-primary" type="reset">Reset</button>
        </div>
    
  </div></form>
      </div>
@endsection