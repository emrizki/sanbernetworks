<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <a href="index.html">Post.fm</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
        <a href="index.html">Pf</a>
    </div>
    <ul class="sidebar-menu">
        <li class="nav-item dropdown">
            <a href="" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="/timeline">Home</a></li>
                <li><a class="nav-link" href="/empty">setting</a></li>
            </ul>
        </li>
        <li class="menu-header">Starter</li>
        <li class="nav-item dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Post</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="/posts/create">New post</a></li>
                <li><a class="nav-link" href="/posts">Post</a></li>
            </ul>
        </li>
        <li class="nav-item dropdown">
            <a href="/profile" class="nav-link has-dropdown"><i class="fas fa-th"></i> <span>Profile</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="/profile">You profile</a></li>
                <li><a class="nav-link" href="/profile/edit">New Profile</a></a></li>
            </ul>
        </li>

    <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
        <a href="https://getstisla.com/docs" class="btn btn-primary btn-lg btn-block btn-icon-split">
        <i class="fas fa-rocket"></i> Documentation
        </a>
    </div>
</aside>