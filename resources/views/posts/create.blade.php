@extends('stisla.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
<div class="card col-8">
<div class="card-header d-flex flex-column justify-content-center">
<h1 style="margin-top: 50px;">Create New Post</h1>
<form action="/posts" method="POST" enctype="multipart/form-data">
  @csrf
  <div class="form-group" style="margin-top: 20px;">
    <label for="body">Body:</label><br>
    <textarea name="body" id="" cols="50" rows="5">{{ old('body', '')}}</textarea>
    @error('body')
      <div class="alert alert-warning">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label for="image">Image</label>
    <input type="file" name="image" class="form-control" id="image" placeholder="Image..">
  </div>
  <div class="form-group">
    <label for="caption">Caption</label><br>
    <textarea name="caption" id="" cols="50" rows="10" placeholder="Caption..."></textarea>
  </div>
  <div class="form-group">
    <label for="quote">Quote</label><br>
    <textarea name="quote" id="" cols="50" rows="10" placeholder="Quote..."></textarea>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
</div>
</div>
@endsection
