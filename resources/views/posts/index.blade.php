@extends('stisla.master')

@section('content')
@foreach($posts as $key => $post)
<div class="card" style="width: 18rem; margin-top: 70px;">
  <img class="card-img-top" src="{{ $post->image }}" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">{{ $post->body}} </h5>
    <p class="card-text">{{ $post->caption }}</p>
    <p class="card-text">{{ $post->quote }}</p>
    <a href="#" class="btn btn-primary">Edit</a>
    <a href="#" class="btn btn-danger">Delete</a>
  </div>
</div>
@endforeach
@endsection