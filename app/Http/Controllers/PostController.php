<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PostController extends Controller
{
    public function create() {
        return view('posts.create');
    }

    public function store(Request $request) {

        $request->validate([
            "body" => "required|unique:posts"
        ]);

        $query = DB::table('posts')->insert([
            "body" => $request["body"],
            "image" => $request["image"],
            "caption" => $request["caption"],
            "quote" => $request["quote"]
        ]);

        $imageName = time().'.'.$request->image->extension();
        $request->image->move(public_path('images', $imageName));
        return redirect('/posts/create');
    }

    public function index() {
        $posts = DB::table('posts')->get();

        return view('posts.index', compact('posts'));
    }
}

