<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;


class ProfileController extends Controller
{
  
    public function index(){
        $profile= DB::table('users')-> where('id',1)->first();
        //dd($profile);
        return view('profile.index',compact('profile'));
    }
 
    public function edit(){
        $profile= DB::table('users')-> where('id',1)->first();
        return view('profile.edit',compact('profile'));
    }
    public function update(Request $request){
        $query = DB::table('users')
        ->where('id', 1)
        ->update([
            "full_name" => $request["full_name"],
            "username" => $request["username"],
            "email" => $request["email"],
            "photo" => $request["photo"]
        ]);
        if( $request->hasFile('photo') ){
            $file = $request->file('photo');
            $ext = $request->photo->getClientOriginalExtension();
            $filename = Carbon::now() . '.' . $ext;
            $place = 'public/files/' . $filename;
            Storage::put($place, File::get($file));
            $users->photo = $filename;
        }
        $users->save();
        //$imageName = time().'.'.$request->image->extension();
        //$request->image->move(public_path('images', $imageName));
        return redirect('/profile')->with('success','profile berhasil diupdate!');
    }
    
     
}
