<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/timeline', function () {
    return view('timeline.index');
});
Route::get('/empty', function () {
    return view('timeline.empty');
});

Route::get('/posts/create', 'PostController@create');
Route::post('/posts', 'PostController@store');
Route::get('/posts', 'PostController@index');


//bagian profile
Route::get('/profile', 'ProfileController@index');
Route::get('/profile/edit','ProfileController@edit');
Route::put('/profile','ProfileController@update');

// Route::get('/halo', function () {
//     return view('sosmed.index');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

